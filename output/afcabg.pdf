%document options
\documentclass[mathserif]{beamer}
\usetheme{boxes}
\usecolortheme{beaver}
\usefonttheme{serif}


\usepackage{natbib}
\usepackage{stmaryrd} %more maths
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{tikz} %drawing package
    \usetikzlibrary{trees,decorations}
\usepackage{color}
\usepackage[T1]{fontenc} 
\usepackage{textcomp} %more symbols
\usepackage{palatino}
\usepackage{lingmacros}
\usepackage{appendixnumberbeamer}

\setcitestyle{notesep={:~}}
\setlength{\bibsep}{0.01cm}

\title[Arundale's face constitution as Bayesian game]{Arundale's face
constitution as Bayesian game}
\author[Lochlan Morrissey]
{Lochlan~Morrissey}
\institute[Griffith University]
{School of Languages and Linguistics\\Griffith University}
\date{ALS 2013\\\(04\cdot10\cdot2013\)}

\begin{document}
\frame{
    \maketitle
}
\frame{
    \frametitle{Aims}
    \begin{itemize}
        \item Formalisation of Arundale's (1999, 2006, 2008, 2010) Face
        Constituting Theory (FCT) into a game theoretic framework
        \item Develop the conjoint co-constituting model (CCM) into a game
        \item Define face as the utility of such games
    \end{itemize}
    }
\frame{
    \frametitle{Arundale's reformulation of face}
    \begin{itemize}
        \item Arundale argues against the classical notion of face as ``the
        positive social value
        a person effectively claims for himself \dots\ an image [that is] self
        delineated in terms of approved social attributes''
        \citep[222]{goffman1967interaction}
        \item Instead, face is a dialectic of social closeness and separateness
        between two (or more) interlocutors, as understood by the
        interlocutors, which
        can be extrapolated to inclusion or exclusion from a social system
        \citep[204--5]{arundale2006face}
        \item Informed by a shift of the ideology of interaction to one in
        which meanings are interactionally achieved by interlocutors
        \citep[cf.][]{arundale1999alternative}
    \end{itemize}
    }
\frame{
    \frametitle{Conjoint co-constituting model (CCM)}
    \begin{itemize}
        \item Two interlocutors, Amy and Bob co-constitute meaning thus:
        \begin{enumerate}{\footnotesize
            \item Amy, based on her beliefs about Bob and the frame of their
            interaction, produces a `position utterance', whose interpretation
            is \emph{provisional}.
            \item Bob selects an interpretation, and, based on his beliefs about
            Amy's intended interpretation and the frame of their interaction,
            produces a position utterance, whose interpretation is still
            provisional, which provides evidence to Amy, and which either
            affirms or disaffirms her expected interpretation.
            \item Amy adds a third position utterance, which confirms an
            interpretation of the original utterance (regardless of whether it
            is the interpretation that she intended or anticipated) as
            \emph{operative}.}
        \end{enumerate}
        \item Face ``is one key aspect of the relationship two or more
        persons conjointly co-constitute as they conjointly co-constitute
        meanings and actions in talk-in-interaction''
        \citep[2085]{arundale2010constituting}

    \end{itemize}
    }
\frame{
    \frametitle{Overview of game theory (GT)}
    \begin{itemize}
        \item Cf.
        \citet{benz2006introduction,jager2008applications,benz2011language}
        \item A game is a set of rules which restricts the possible actions
        performed by its players, and a mechanism which distributes winnings
        and losses according to the players' chosen actions
        \item Expressed formally as \[G\equiv(N,(\mathcal{A}_i)_{i\in N},U)\]
        where
        \begin{itemize}
            \item \(N=\{1,\dots,n\}\) is the set of players \(1,\dots,n\);
            \item \(\mathcal{A}_i\) is the set of actions available to player
            $i$, called $i$'s \emph{action profile}; and
            \item \(U:\mathcal{A}\longrightarrow\mathbb{R}^n\) is a function
            that maps action profiles \((a_1,\dots,a_n)\) to \(n\)-tuples of
            real-number-valued \emph{utilities}, or \emph{payoffs},
            \((u_1,\dots,u_n)\).
        \end{itemize}
    \end{itemize}
    }
\frame{
    \frametitle{GT: Preferences and strategy --- coin-matching}
    \begin{itemize}
        \item Amy and Bob hold a coin each. They select a side, heads or tails,
        and reveal the coins to one another. Amy wins Bob's coin if both coins
        are matching, and Bob wins if they are not.
    \end{itemize}
    \begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
     & $h$ & $t$ \\ \hline
    $h$ & $(1\; ; \; -1)$ & $(-1\; ;\; 1)$ \\ \hline
    $t$ & $(-1\; ; \; 1)$ & $(1\; ;\; -1)$\\ \hline
    \end{tabular}
    \label{utab:amybob}
    \end{table}
    \begin{itemize}
        \item While each player can prefer a certain outcome---$(h,h)$ or
        $(t,t)$ for Amy, $(h,t)$ or $(t,t)$ for Bob---there is no way they can
        make sure of their preferred outcome
        \item Preference of the utility of $(h,h)$ to $(h,t)$ is expressed as
        $$u_a(h,t)\prec_au_a(h,h)$$
    \end{itemize}
    }
\frame{
    \begin{itemize}
        \item Players can choose to defect $d$ or remain silent $r$.
    \end{itemize}
    \frametitle{GT: Preferences and strategy --- prisoner's dilemma}
    \begin{table}[h]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
     & $d$ & $r$ \\ \hline
    $d$ & $(-7\; ; \; -7)$ & $(-10\; ;\; 0)$ \\ \hline
    $r$ & $(0\; ; \; -10)$ & $(-2\; ;\; -2)$\\ \hline
    \end{tabular}
    \end{table}
    \begin{itemize}
        \item A \emph{strategy} $S_i$ is a function which maps an action
        $a_i\in\mathcal{A}_i$ to every $a_j\in\mathcal{A}_j$
        \item A \emph{strategy profile} $s$ is an $n$-tuple of the strategies of
        each player, $s=(s_1,\dots,s_n)$; player $i$'s strategy profile is
        denoted as $(s_i,s_{-i})$
        \item If $(s'_i,s_{-i})\prec_i(s_i,s_{-i})$: $(s_i,s_{-i})$
        \emph{strictly dominates} $(s'_i,s_{-i})$
    \end{itemize}
    }
\frame{
    \frametitle{GT: Preference and strategy --- Nash equilibria}
        $$[u_a(d,r)=-10]\prec_a[u_a(d,d)=-7]\prec_a[u_a(r,r)=-2]\prec_a[u_a(r,d)=0]$$
    \begin{itemize}
        \item \textbf{However!} The same preferences are true of Bob, and Amy
        knows it
        \item $\therefore$ players choose $(d,d)$.
        \item A strategy profile $s=(s_1,\dots,s_n)$ is a \emph{Nash
        equilibrium} if it is not in player $i$'s interest to choose another
        strategy, given the strategies of the $n-1$ other players 
        \item for every player $i$, it holds that there does not exist a strategy profile $s$
        such that $s\prec_i(s'_i,s_{-i})$
    \end{itemize}
    }
\frame{
    \frametitle{GT: Games of incomplete information}
    \begin{itemize}
        \item A game $G$ is a \emph{game of incomplete information} if at least
        one player lacks knowledge of at least one other player's utilities,
        and therefore preferences
        \item Additional specifications:
        \begin{enumerate}[(a)]
            \item player $i$ has a \emph{type} $t_i\in\mathcal{T}_i$, known only to $i$
            \item $i$'s utility becomes $u(a_1,\dots,a_n;t_i)$
            \item $i$ has \emph{beliefs} about the types of the $n-1$ other
            players, based on her own; her beliefs of player $j$'s type $t_j$
            is represented as a conditional probability $P(t_j|t_i)$, which is an element of a set
            $\mathcal{P}$
            \item utilities are expressed as \emph{expected utilities}
            $\mathring{u}_i$
        \end{enumerate}
        $$G\equiv(N,(\mathcal{A}_i)_{i\in N},\mathcal{T},\mathcal{P},U)$$
    \end{itemize}
    }
\frame{
    \frametitle{GT: Signaling games}
    \begin{enumerate}
        \item player $0$ assigns a type $t$ the probability of which, $\rho$, is common
        knowledge, and reveals it only to player $1$.
        \item player $1$, having viewed $t$, sends a message $m\in M$ to player $2$.
        \item player $2$, having received $m$, infers what $t$ is, and performs an
        action $a\in\mathcal{A}_2$.
    \end{enumerate}
    \begin{figure}[h]
    \centering
    \input{figs/signal.tex}
    \end{figure}
    }
\frame{
    \frametitle{GT: Signaling games --- quiche or beer?}
    \begin{figure}
    \centering
    \input{figs/beerquiche.tex}
    \end{figure}
    }
\frame{
    \frametitle{GT: Signaling games --- Bayesian inference}
        $$P(A|B)=\frac{P(B|A)P(A)}{P(B)}$$
    \begin{itemize}
        \item What is the probability that $j$ is wimpy if he serves quiche?
        \begin{enumerate}[(a)]
            \item $\rho=0.5$ and therefore $1-\rho=0.5$
            \item $P(B)=0.5=P(Q)$
            \item $P(Q|t_1)=0.8,P(B|t_1)=0.2;P(B|t_2)=0.8,P(Q|t_2)=0.2$
        \end{enumerate}
    \end{itemize}
        $$\therefore P(t_1|Q)=\frac{P(Q|t_1)\times
        P(t_1)}{P(Q)}=\frac{0.8\times0.5}{0.5}=0.8$$
    }
%\frame{
%    \frametitle{GT: Games of partial information}
%    \begin{itemize}
%    \item Formulated by \citet{parikh1991communication,parikh2000communication,parikh2006pragmatics,parikh2006radical,parikh2007situations}
%    \item Places interpretative act explicitly within the game
%    \item Includes `cost' of moves being included in calculation of utilities
%    \end{itemize}
%    \begin{figure}
%    \centering
%    \input{../figs/partial.tex}
%    \end{figure}
%    }
\frame{
    \frametitle{Formalisation of the CCM}
    \begin{itemize}
    \item Basic ternary structure, with the following moves:
    \begin{enumerate}[i.]
    \item $1$, based on her beliefs about $2$ and the frame of their
    interaction, produces a position utterance.
    \item $2$ selects an interpretation, and, based on her beliefs about $1$'s
    intended interpretation and the frame of their interaction, produces a
    position utterance, which provides evidence to $1$, and which either affirms or
    disaffirms her expected interpretation.
    \item $1$ adds a third position utterance, which confirms an interpretation
    of the original utterance (regardless of whether it is the interpretation
    that she intended or anticipated) as operative.
    \end{enumerate}
    \end{itemize}
    }
\frame{
    \frametitle{Formalisation of the CCM --- extensive form}
    \begin{figure}
    \centering
    \input{figs/coconstituting.tex}
    \end{figure}
    \[
        u\preceq_{1,2}u'\preceq_{1,2}u'''\preceq_{1,2}u''
    \]

    }
\frame{
    \frametitle{Face, utility and functional achievements}
    From \citet[86]{haugh2010jocular}:
    \enumsentence{\label{ex:noeat}
                Attendant: \shortex{5}
                {\emph{Mooshiwake} & \emph{gozai-mas-en} & \dots\ &
                \emph{mooshiwake} & \emph{gozai-mas-en} \dots}
                {excuse(Pol) & have-Pol-Neg & & excuse(Pol) & have-Pol-Neg}
                {`I am very sorry \dots\ I am very sorry \dots{}'}
                \item Visitor: \shortex{2}
                {\emph{A',} & \emph{ike-nai}?}
                {oh & acceptable-Neg}
                {`Oh, is this not allowed?'}
                \item Attendant: \shortex{2}
                {\emph{Mooshiwake} & \emph{gozai-mas-en} \dots}
                {excuse(Pol) & have-Pol-Neg}
                {`I am very sorry\dots'}
             }
    }
\frame{
    \frametitle{Composite games}
    A game $\gamma$ is a \emph{composite game} if, for $k$ games
    $G_1,\dots,G_k$, called subgames of $\gamma$:
    \begin{enumerate}[(1)]
        \item the utility function \(\omega\) of \(\gamma\) for player $i$ is
            the sum of $i$'s utilities of
            its subgames;
%            \[\omega_i^\gamma=\sum_{G\in\gamma}u_i^{G_1},\dots,u_i^{G_k}\]
        \item the set of players $N$ is the same for every subgame;
        \item the action profile $\mathcal{A}$ is the same for every subgame.
    \end{enumerate}
    }
\frame{
    \frametitle{Face, utility and functional achievements}
    \begin{figure}
    \centering
    \input{figs/cosig.tex}
    \end{figure}
    }
\frame{
    \frametitle{Face, utility and functional achievements}
    \begin{itemize}
      \item Overall, we can assume that both players $a$ and $v$ would prefer their
          composite utilities, \(\omega^\gamma_a\),\(\omega^\gamma_v\), to be
          greater than zero, and consider such an outcome `successful'.
      \item Players are able to `weigh' utilities according to their objective
          of the interaction.
        \begin{itemize}
              \item In the above example: player $a$'s objective is
                  functional, therefore \(u_a^{G_2}>u_a^{G_1}\).
        \end{itemize}
    \end{itemize}
    }
\frame{
    \frametitle{Politeness}
    How can we account for $a$'s politeness in this example?
    \enumsentence{\label{ex:noeat}
                Attendant ($a$): \shortex{5}
                {\emph{Mooshiwake} & \emph{gozai-mas-en} & \dots\ &
                \emph{mooshiwake} & \emph{gozai-mas-en} \dots}
                {excuse(Pol) & have-Pol-Neg & & excuse(Pol) & have-Pol-Neg}
                {`I am very sorry \dots\ I am very sorry \dots{}'}
                \item Visitor ($v$): \shortex{2}
                {\emph{A',} & \emph{ike-nai}?}
                {oh & acceptable-Neg}
                {`Oh, is this not allowed?'}
                \item Attendant ($a$): \shortex{2}
                {\emph{Mooshiwake} & \emph{gozai-mas-en} \dots}
                {excuse(Pol) & have-Pol-Neg}
                {`I am very sorry\dots'}
             }
    }
\frame{
    \frametitle{Politeness}
    \begin{itemize}
        \item $a$'s request of $v$ does not increase closeness between the two
            interlocutors, and therefore the utility for both players for $G_1$
            is less than zero.
        \item If \((u_{a,v}^{G_1}+u_{a,v}^{G_1})<0\), the interaction is
            unsuccessful.
        \item Politeness is, in this model, an attempt to \emph{mitigate the
            negative utility of a subgame} so that the net result of the
            composite game is positive.
    \end{itemize}
    }
\frame{
    \frametitle{How does $v$ construct a successful interaction?}
    \begin{itemize}
        \item \(v\)'s preferred action in \(G_2\) is \(a_1\), to continue
            eating, over \(a_2\), to stop eating.
        \item \(v\)'s separateness from \(a\) caused by the request in \(G_1\),
            plus the utility of \(G_2\)

            \vfill

            \begin{center}

            \(\hookrightarrow\) unsuccessful interaction
            
            \end{center}

        \item How does (can) $v$ successfully construct an interaction?
    \end{itemize}
    }
\frame{
    \frametitle{Successful interaction for $v$}
    Scenario 1:
    \begin{itemize}
        \item $v$ selects $a_1$ (keep eating).
            \begin{itemize}
                \item $u_v^{G_2}$ is high.
                \item $u_a^{G_1}$ is low.
            \end{itemize}
    \end{itemize}
    Scenario 2:
    \begin{itemize}
        \item $v$ selects $a_2$ (stop eating).
            \begin{itemize}
                \item $u_v^{G_2}$ is low.
                \item $u_a^{G_1}$ is high.
            \end{itemize}
    \end{itemize}
    $\therefore$ $v$ will choose $a_1$

    \textbf{However!} We can assume that she will, in reality, not choose this.
    Why?
    }
\frame{
    \frametitle{The game(s) never end(s)!}
    \begin{itemize}
        \item Games can be added \emph{ad infinitum}.
    \end{itemize}
    \begin{itemize}
        \item For $\gamma$ to be successful, $v$ must realise that there will
            be games following from $G_2$, the negative utilities of
            which may far exceed the positive utility of $u_v^{G_2}(a_2)$
   \end{itemize}
     \vfill
    \begin{figure}
        \centering
        \begin{tikzpicture}
            \input{figs/infinitum.tex}
        \end{tikzpicture}
    \end{figure}
}
\appendix
\begin{frame}[allowframebreaks]{References}
    \footnotesize
    \bibliography{../biblio.bib}
    \bibliographystyle{apalike}
\end{frame}

\end{document}

