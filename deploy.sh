#!/bin/sh

curl https://gitlab.com/lochland/output/raw/master/lmorrissey-output.yaml -o _data/output.yaml &&
jekyll build &&
rsync -crzvv --delete _site/ lochland_lmorrisseyinfo@ssh.phx.nearlyfreespeech.net:/home/public/lmorrissey.info &&
echo "Pushed to server."
git push origin --all
echo "Pushed to origin."
